package ru.t1.godyna.tm.command.system;

import ru.t1.godyna.tm.api.model.ICommand;
import ru.t1.godyna.tm.command.AbstractCommand;

import java.util.Collection;

public final class ArgumentListConnamd extends AbstractSystemCommand {

    private final String NAME = "arguments";

    private final String ARGUMENT = "-arg";

    private final String DESCRIPTION = "Show list arguments";

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        final Collection<AbstractCommand> commands = getCommandService().getCommands();
        for (ICommand command : commands) {
            if (command == null) continue;
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument.toString());
        }
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
