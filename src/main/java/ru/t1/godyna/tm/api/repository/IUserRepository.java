package ru.t1.godyna.tm.api.repository;

import ru.t1.godyna.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(String login);

    User findByEmail(String email);

    Boolean isLoginExist(String login);

    Boolean isMailExist(String email);

}
