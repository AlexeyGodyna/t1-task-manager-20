package ru.t1.godyna.tm.api.repository;

import ru.t1.godyna.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    Project create(String userId, String name, String description);

    Project create(String userId, String name);

}
